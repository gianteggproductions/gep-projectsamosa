<?php
    session_start();
    
    include 'dbconfig.php';
    
    $cleanForm = true;
    
    if ($_POST['username'] == '')
    {
        echo "No Username Received";
        $cleanForm = false;
    }
    else
    {
        $usernameSQL = $mysqlcon->prepare("SELECT name FROM players WHERE name = ?");
        
        if ($usernameSQL->execute(array($_POST['username'])))
        {    
            if ($usernameSQL->rowCount() > 0)
            {
                echo "Username already registered";
                exit;
            }
        }
        else
        {
            echo "Database encountered an error!";
            exit;
        }
    }
        
    if ($_POST['email'] == '')
    {
        echo "No Email Received";
        $cleanForm = false;
    }
    else
    {
        $emailRegex = "/^([a-zA-Z0-9])+([a-zA-Z0-9._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9._-]+)+$/";
        if (preg_match( $emailRegex, $_POST['email'] ))
        {
            $emailSQL = $mysqlcon->prepare("SELECT email FROM players WHERE email = ?");
            
            if ($emailSQL->execute(array($_POST['email'])))
            {        
                if ($emailSQL->rowCount() > 0)
                {
                    echo "Email already in use";
                    exit;
                }
            }
            else
            {
                echo "Database encountered an error!";
                exit;
            }
        }
        else
        {
            echo "Invalid email";
            exit;
        }
    }
        
    if ($_POST['password'] == '')
    {
        echo "No Password Recieved";
        exit;
    }
    
    if ($cleanForm)
    {
        $username = CleanString($_POST['username']);
        $email    = CleanString($_POST['email']);
        $password = CleanString($_POST['password']);
        $com_code = CleanString(md5(uniqid(rand())));

        $registrationSQL = $mysqlcon->prepare("INSERT INTO players (name, email, password, com_code) VALUES (?, ?, ?, ?)");
        
        if ($registrationSQL->execute(array($username, $email, $password, $com_code)))
        {
            $to = $email;
            $subject = "[Giant Egg Productions] Email Verification For $username";
            $header = "From: registrations@gianteggproductions.com \r\n";
            $header .= "Email Activation\r\n";
            $message .= "Follow the link below to activate the email attached to $username's account.\r\n\r\n";
            $message .= "http://gepsamosa.comxa.com/verify.php?passkey=$com_code";

            $sentMail = mail($to,$subject,$message,$header);

            if($sentMail)
            {
                echo "Account Created.\r\nConfirmation email has been sent!";
            }
            else
            {
                echo "Failed to send Confirmation link to your e-mail address.";
            }
        }
        else
        {
            echo "Database encountered an error!";
        }
    }
?>