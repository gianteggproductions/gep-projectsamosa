<?php
    session_start();
    
    include 'dbconfig.php' ;

    $varsSet = false;
        
    if (isset($_POST['username']))
    {
        $username = CleanString($_POST['username']);
    }
    else
    {
        echo "No username given";
        exit;
    }
    
    if (isset($_POST['password']))
    {
        // Shouldn't need to clean the password, as we're recieving an MD5 hash from the client
        $password = $_POST['password'];
        $varsSet = true;
    }
    else
    {
        echo "No password given";
        exit;
    }
    
    if ($varsSet)
    {
        $loginSQL = $mysqlcon->prepare("SELECT id,name,password,currency,rating FROM players WHERE name=? AND password=?");
        
        if (!$loginSQL->execute(array($username, $password)))
        {
            echo "PDO::errorInfo()";
            exit;
        }
    
        if ($loginSQL->rowCount() > 0)
        {        
            $playerStats = $loginSQL->fetch();
            
            header('PlayerID: $playerID');
            
            $xmlOut = new XMLWriter();
                $xmlOut->openURI('php://output');
                $xmlOut->startDocument('1.0', 'UTF-8');
                $xmlOut->setIndent(4);
                $xmlOut->startElement('loginStats');
                $xmlOut->writeElement('id', $playerStats["id"]);
                $xmlOut->writeElement('name', $playerStats["name"]);
                $xmlOut->writeElement('currency', $playerStats["currency"]);
                $xmlOut->writeElement('score', $playerStats["rating"]);
                $xmlOut->endElement();
                $xmlOut->endDocument();
                $xmlOut->flush();
        }
        else
        {
            echo "Username/Password not recognised";
        }
    }
    else
    {
        echo "Failed to parse username & password";
    }
    
?>