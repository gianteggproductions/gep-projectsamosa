<?php
    include('dbconfig.php');
    
    if (isset( $_GET['passkey'] ))
    {        
        $passkey = $_GET['passkey'];
    }
    else
    {
        echo("No Passkey Set");
        exit;
    }
    
    $verifySQL = $mysqlcon->prepare("UPDATE players SET com_code='ACTIVE' WHERE com_code=?");
    
    if($verifySQL->execute(array($passkey)))
    {
        echo 'Email verified';
    }
    else
    {
        echo "Failed to verify your email";
        echo "PDO::errorInfo()";
    }
?>