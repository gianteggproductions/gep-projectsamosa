﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Xml;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LoginController : MonoBehaviour
{
    private Text _txtUsername, _txtPassword, _txtRegUsername, _txtRegPassword, _txtRegEmail, _lblLoginStatus;
    private Button _btnLogin, _btnCreateAccount;
    private Notification _notification;
    private MenuControl _menuControl;
    private const string _sBaseUrl = "http://gepsamosa.comxa.com/";

    private void Start()
	{
	    _txtUsername    = GameObject.Find("txtUsername").GetComponentInChildren<Text>();
	    _txtPassword    = GameObject.Find("txtPassword").GetComponentInChildren<Text>();
        _txtRegUsername = GameObject.Find("txtUsernameRegister").GetComponentInChildren<Text>();
        _txtRegPassword = GameObject.Find("txtPasswordRegister").GetComponentInChildren<Text>();
        _txtRegEmail    = GameObject.Find("txtEmailRegister").GetComponentInChildren<Text>();
        _lblLoginStatus = GameObject.Find("lblLoginStatus").GetComponent<Text>();

	    _btnLogin = GameObject.Find("btnLogin").GetComponent<Button>();
        _btnCreateAccount = GameObject.Find("btnSubmitRegistration").GetComponent<Button>();

        _notification = GameObject.Find("NotificationHandler").GetComponent<Notification>();
        _menuControl = GameObject.Find("UI").GetComponent<MenuControl>();

        if (!PlayerPrefs.HasKey("PlayerID")) return;

        //_menuControl.SetMenu(Menu.MainMenu);
        Debug.Log(PlayerPrefs.GetInt("PlayerID"));
	}

    private static string Md5Sum(string strToEncrypt)
    {

        var encoding = new UTF8Encoding();
        var bytes = encoding.GetBytes(strToEncrypt);

        // Create a new MD5 instance, and hash the password
        var md5 = new MD5CryptoServiceProvider();
        var hashBytes = md5.ComputeHash(bytes);

        // Convert the encrypted bytes back to a string (base 16)
        var hashString = hashBytes.Aggregate("", (current, t) => current + Convert.ToString(t, 16).PadLeft(2, '0'));

        return hashString.PadLeft(32, '0');
    }

	private void Update() 
    {
        if (_btnLogin == null) _btnLogin = GameObject.Find("btnLogin").GetComponent<Button>();
        if (_btnCreateAccount == null) _btnCreateAccount = GameObject.Find("btnSubmitRegistration").GetComponent<Button>();

	    if (String.IsNullOrEmpty(_txtUsername.text) || String.IsNullOrEmpty(_txtPassword.text))
	    {
	        _btnLogin.interactable = false;
	    }
	    else
	    {
	        _btnLogin.interactable = true;
	    }

	    if (String.IsNullOrEmpty(_txtRegUsername.text) || String.IsNullOrEmpty(_txtRegPassword.text) ||
	        String.IsNullOrEmpty(_txtRegEmail.text))
	    {
	        _btnCreateAccount.interactable = false;
	    }
	    else
	    {
	        _btnCreateAccount.interactable = true;
	    }
	}

    private void OnApplicationExit()
    {
        Debug.Log("Removing Keys");
        PlayerPrefs.DeleteKey("PlayerID");
        PlayerPrefs.DeleteKey("PlayerName");
    }

    public void Login()
    {
        var sPassword = Md5Sum(_txtPassword.text);
        StartCoroutine(LoginEnumerator(_txtUsername.text, sPassword));
    }

    private IEnumerator LoginEnumerator(string sUsername, string sPassword)
    {
        if (Network.player.ipAddress == "127.0.0.1")
        {
            _lblLoginStatus.text = "There doesn't seem to be an internet connection";
            _lblLoginStatus.color = new Color(150,0,0);
            yield break;
        }

        Debug.Log(String.Format("Username: {0}, Password: {1}", sUsername, sPassword));
        _lblLoginStatus.text = "Logging In...";
        _lblLoginStatus.color = new Color(50,200,50);

        const string sLoginUrl = _sBaseUrl + "login.php";

        var loginForm = new WWWForm();
        loginForm.AddField("username", sUsername);
        loginForm.AddField("password", sPassword);
        
        var loginHeaders = new Dictionary<string, string> {{"PlayerID", "null"}};

        var phpResult = new WWW(sLoginUrl, loginForm.data, loginHeaders);

        yield return phpResult;

        if (phpResult.error != null)
        {
            _lblLoginStatus.text = "There was an error logging in";
            _lblLoginStatus.color = new Color(150,0,0);
            Debug.Log(phpResult.error);
            yield break;
        }

        if (phpResult.text.Contains("Username/Password not recognised") || !phpResult.text.Contains("xml"))
        {
            _lblLoginStatus.text = "Invalid login credentials";
            _lblLoginStatus.color = new Color(150,0,0);

            Debug.Log(phpResult.text);
        }
        else
        {
            var xmlReader = new XmlDocument();

            // Using 000webhost sometimes generates ad crap at the end of a PHP result, so remove it
            var playerStats = phpResult.text.Contains("<!--") ? phpResult.text.Substring(0, phpResult.text.IndexOf("<!--", StringComparison.Ordinal)) : phpResult.text;

            xmlReader.LoadXml(playerStats);

            Debug.Log(playerStats);

            var xmlNodeList = xmlReader.SelectNodes("//loginStats");

            if (xmlNodeList == null) yield break;

            var userStats = xmlNodeList[0];
            if (userStats != null)
            {
                PlayerPrefs.SetInt("PlayerID", int.Parse(userStats.ChildNodes[0].InnerText));
                PlayerPrefs.SetString("PlayerName", userStats.ChildNodes[1].InnerText);
                PlayerPrefs.SetInt("GEP_PlayerYolk", int.Parse(userStats.ChildNodes[2].InnerText));
                PlayerPrefs.SetFloat("GEP_PlayerRating", float.Parse(userStats.ChildNodes[3].InnerText));

                _lblLoginStatus.text = "Successfully logged in as " + userStats.ChildNodes[1].InnerText;
                _lblLoginStatus.color = new Color(0,200,0);

                yield return new WaitForSeconds(2f);

                StartCoroutine(_menuControl.HandleGameTitle());
                _menuControl.FadeMenu(Menu.MainMenu);
            }
            else
            {
                _lblLoginStatus.text = "There was an error logging in, please try again";
                _lblLoginStatus.color = new Color(150,0,0);
            }
        }
    }

    public void Register()
    {
        StartCoroutine(RegisterEnumerator(_txtRegUsername.text, Md5Sum(_txtRegPassword.text), _txtRegEmail.text));
    }

    private IEnumerator RegisterEnumerator(string sUsername, string sPassword, string sEmail)
    {
        const string sRegisterUrl = _sBaseUrl + "register.php";

        var registerForm = new WWWForm();
        registerForm.AddField("username", sUsername);
        registerForm.AddField("password", sPassword);
        registerForm.AddField("email", sEmail);

        var phpResult = new WWW(sRegisterUrl, registerForm);

        yield return phpResult;

        var cleanResult = phpResult.text.Contains("<!--") ? phpResult.text.Substring(0, phpResult.text.IndexOf("<!--", StringComparison.Ordinal)) : phpResult.text;

        _notification.DisplayNotification(phpResult.error ?? cleanResult);
        Debug.Log(cleanResult);
    }
}
