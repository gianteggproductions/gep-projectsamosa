﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MenuNavigation : MonoBehaviour
{
    private List<GameObject> _inputFields;
    private int _curIndex;

    private GameObject _gAccountHolder;
    private Button _btnLogin;
    private CanvasGroup _cvsAccount;
    private EventSystem _eventSystem;

    private MenuControl _menuControl;

    private void Start()
    {
        _gAccountHolder = GameObject.Find("Account");
        _btnLogin       = _gAccountHolder.transform.Find("Login/btnLogin").GetComponent<Button>();
        _cvsAccount     = _gAccountHolder.GetComponent<CanvasGroup>();
        _eventSystem    = EventSystem.current;

        _menuControl = GameObject.Find("UI").GetComponent<MenuControl>();

        _inputFields = new List<GameObject>();

        RefreshInputs(_gAccountHolder.transform.Find("Login").gameObject);
    }

    private void Update () 
    {
	    HandleKeyInput();
	}

    private void HandleKeyInput()
    {
        if (Math.Abs(_cvsAccount.alpha - 1) > 0.1) return;

        var pointer = new PointerEventData(EventSystem.current);

        if (Input.GetKeyDown(KeyCode.Tab))
        {
            if (_inputFields.Count == 0)
            {
                Debug.Log("No Inputs Found");
                return;
            }

            if (_curIndex >= _inputFields.Count) _curIndex = 0;

            _eventSystem.SetSelectedGameObject(_inputFields[_curIndex], new BaseEventData(_eventSystem));
            Debug.Log("Clicked Index: " + _curIndex);
            _curIndex++;
        }
        else if (Input.GetKeyDown(KeyCode.Return))
        {
            Debug.Log("Entered");
            if (_btnLogin.interactable == false) return;

            ExecuteEvents.Execute(_btnLogin.gameObject, pointer, ExecuteEvents.submitHandler);
        }
    }

    public void RefreshInputs(GameObject parentGameObject)
    {
        _curIndex = 0;
        _inputFields.Clear();

        foreach (var inputField in parentGameObject.GetComponentsInChildren<InputField>())
        {
            _inputFields.Add(inputField.gameObject);
            Debug.Log("Added Input at Index: " + _inputFields.IndexOf(inputField.gameObject));
        }

        Debug.Log("Refreshed Inputs New Count: " + _inputFields.Count);
    }
}
