﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class NotificationButtonHandler : MonoBehaviour 
{

	void Start ()
	{
	    GetComponent<Button>().onClick.AddListener(RemoveNotification);
	}

    private void RemoveNotification()
    {
        GameObject.Find("NotificationHandler").GetComponent<Notification>().DisplayNext();
        Destroy(transform.parent.transform.parent.gameObject);
    }
}
