﻿using System;
using UnityEngine;
using System.Collections;

public class NotificationHandler : MonoBehaviour
{

    public bool isActive;

    private CanvasGroup _cvsNotification;

	void Start () {
	    _cvsNotification = gameObject.GetComponent<CanvasGroup>();
	}
	
	void Update () {
        if (isActive && _cvsNotification.alpha != 1)
        {
            _cvsNotification.alpha = 1;
            _cvsNotification.blocksRaycasts = true;
        }
        else if (!isActive && _cvsNotification.alpha != 0)
        {
            _cvsNotification.alpha = 0;
            _cvsNotification.blocksRaycasts = false;
        }
	}
}
