﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Notification : MonoBehaviour
{
    private GameObject _gNotificationPrefab;
    private Dictionary<int, GameObject> _gNotificationQueue; 

    private void Start()
    {
        _gNotificationPrefab = Resources.Load<GameObject>("Prefabs/UI/Notification");
        _gNotificationQueue = new Dictionary<int, GameObject>();
    }

    public void DisplayNotification(string sNotificationText)
    {
        if (_gNotificationQueue.Count > 0)
        {
            if (_gNotificationQueue.Select(entryPair => entryPair.Value.transform.Find("pnlNotify/txtNotifyText").GetComponent<Text>().text).Any(notificationText => notificationText == sNotificationText))
            {
                return;
            }
        }

        var pnlNotify = Instantiate(_gNotificationPrefab, Vector3.zero, Quaternion.identity) as GameObject;

        if (pnlNotify == null) return;

        pnlNotify.transform.localScale = Vector3.one;
        pnlNotify.transform.SetParent(GameObject.Find("UI").transform, true);
        pnlNotify.GetComponent<RectTransform>().anchoredPosition = new Vector2(10,10);
        pnlNotify.transform.Find("pnlNotify/txtNotifyText").GetComponent<Text>().text = sNotificationText;

        var newIndex = 1;

        if (_gNotificationQueue.Count != 0)
        {
            newIndex = _gNotificationQueue.Keys.Max() + 1;
        }

        _gNotificationQueue.Add(newIndex, pnlNotify);

        Debug.Log("Notification Index: " + newIndex);
    }

    private void Update()
    {
        if (_gNotificationQueue.Count == 0) return;

        var minIndex = _gNotificationQueue.Keys.Min();
        var notificationIndex = _gNotificationQueue[minIndex].GetComponent<NotificationHandler>();

        if (!notificationIndex.isActive)
        {
            notificationIndex.isActive = true;
        }
    }

    public void DisplayNext()
    {
        _gNotificationQueue.Remove(_gNotificationQueue.Keys.Min());

        if (_gNotificationQueue.Count == 0) return;

        _gNotificationQueue[_gNotificationQueue.Keys.Min()].GetComponent<NotificationHandler>().isActive = true;
    }
}
