﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ManaController : MonoBehaviour
{
    public GameObject gTextObject;

    private void Start()
    {
        gameObject.GetComponent<Slider>().onValueChanged.AddListener(OnSliderValueChanged);
    }

    private void OnSliderValueChanged(float fValue)
    {
        gTextObject.GetComponent<Text>().text = Mathf.Ceil(fValue).ToString();
    }
}
