﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ServerPanelHandler : MonoBehaviour
{
    public GameObject gBtnServerPrefab;

    private const float _fBtnPaddingX = 10f, _fBtnPaddingY = -10f;
    private List<GameObject> _gServerButtons;
    private Text _tBrowserStatus, _tServerToggle, _tServerName;
    private PlayerNetworkManager _pNetManager;

    private void Start()
    {
        _gServerButtons = new List<GameObject>();
        _tBrowserStatus = GameObject.Find("txtBrowserStatus").GetComponent<Text>();
        _tServerToggle  = GameObject.Find("btnToggleServer").GetComponentInChildren<Text>();
        _tServerName    = GameObject.Find("txtServerName").GetComponentInChildren<Text>();
        _pNetManager    = GameObject.Find("UI").GetComponent<PlayerNetworkManager>();
    }

    private void Update ()
	{
	    var btnAmount = _gServerButtons.Count;
	    var iPreferredHeight = (32*btnAmount) + 20;

	    var rtPanelComp = gameObject.GetComponent<RectTransform>();
	    if (iPreferredHeight > rtPanelComp.sizeDelta.y)
	    {
            rtPanelComp.sizeDelta = new Vector2(rtPanelComp.sizeDelta.x, iPreferredHeight);
            rtPanelComp.anchoredPosition = new Vector2(0, -(iPreferredHeight/2));
	    }
	}

    public void ClearServerList()
    {
        foreach (var btnServer in _gServerButtons)
        {
            Destroy(btnServer);
        }
        _gServerButtons.Clear();
    }

    public void AddNewServer(HostData hostData)
    {
        Debug.Log(String.Format("Handled Server '{0}'", hostData.gameName));
        var btnServer = Instantiate(gBtnServerPrefab, Vector3.zero, Quaternion.identity) as GameObject;

        if (btnServer == null) return;

        btnServer.transform.parent = gameObject.transform;

        var btnRect = btnServer.GetComponent<RectTransform>();
        btnRect.localScale = new Vector3(1,1,1);
        btnRect.anchoredPosition = new Vector2(_fBtnPaddingX, _fBtnPaddingY - (32 * _gServerButtons.Count));
        btnServer.GetComponentInChildren<Text>().text = hostData.gameName;
        btnServer.GetComponent<ButtonServerInfo>().hostData = hostData;

        _gServerButtons.Add(btnServer);
    }

    public void SetBrowserStatusText(string statusText)
    {
        _tBrowserStatus.text = statusText;
    }

    public void SetServerToggleText(string toggleText)
    {
        _tServerToggle.text = toggleText;
    }

    public void ToggleServer()
    {
        switch (_tServerToggle.text)
        {
            case "Start Server":
                _pNetManager.StartServer(_tServerName.text.Trim());
                break;
            case "Stop Server":
                _pNetManager.StopServer();
                break;
        }
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
