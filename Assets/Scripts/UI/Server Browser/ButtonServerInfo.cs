﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ButtonServerInfo : MonoBehaviour
{

    public HostData hostData;

    private PlayerNetworkManager _pNetManager;

    private void Start()
    {
        _pNetManager = GameObject.Find("UI").GetComponent<PlayerNetworkManager>();
        gameObject.GetComponent<Button>().onClick.AddListener(() => _pNetManager.ConnectToServer(hostData));
    }

}
