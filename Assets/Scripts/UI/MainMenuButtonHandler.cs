﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MainMenuButtonHandler : MonoBehaviour {

    void Start( )
    {
        gameObject.GetComponent<Button>().onClick.AddListener(OnClick);
    }

    private void OnClick( )
    {
        GameObject.Find("UI").GetComponent<MenuControl>().ChangeMenu(Menu.MainMenu);
    }
}
