﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public enum Player
{
    One,
    Two
}

public class PlayerUIControl : MonoBehaviour
{

    private enum AnimType
    {
        Activate,
        Deactivate
    }

    public GameObject gPlayerOneUI, gPlayerTwoUI;

    private const float _fAnimateTime = 0.6f;
    private bool _bAnimating = false;

    public void ActivatePlayer(Player player)
    {
        StartCoroutine(Animate(player, AnimType.Activate));
    }

    public void DeactivatePlayer(Player player)
    {
        StartCoroutine(Animate(player, AnimType.Deactivate));
    }

    private IEnumerator Animate(Player player, AnimType animType)
    {
        if (_bAnimating) yield return null;

        _bAnimating = true;

        var vEndPos = Vector2.zero;
        GameObject playerToAnimate = null;

        switch (player)
        {
            case Player.One:
            {
                float xValue = animType == AnimType.Activate ? -200 : -450;
                vEndPos = new Vector2(xValue, 60);

                playerToAnimate = gPlayerOneUI;
            }
            break;
            case Player.Two:
            {
                float xValue = animType == AnimType.Activate ? 200 : 450;
                vEndPos = new Vector2(xValue, 60);

                playerToAnimate = gPlayerTwoUI;
            }
            break;
        }

        float fStartTime = Time.time;

        while (Time.time < fStartTime + _fAnimateTime)
        {
            if (playerToAnimate != null)
            {
                var rRectTransform = playerToAnimate.GetComponent<RectTransform>();
                var vAnchoredPos = rRectTransform.anchoredPosition;

                rRectTransform.anchoredPosition = Vector2.Lerp(
                    vAnchoredPos, vEndPos,
                    (Time.time - fStartTime) / _fAnimateTime);
            }

            yield return null;
        }

        _bAnimating = false;
    }
}
