﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ButtonHandlers : MonoBehaviour
{

    private Button _gMainMenu, _gMultiPlayer, _gBackToMenu, _gRegister, _gLogin, _gCustomise;
    private MenuControl _menuControl;

	void Start ()
	{
	    _menuControl = GameObject.Find("UI").GetComponent<MenuControl>();

	    _gMultiPlayer   = GameObject.Find("btnMultiplayer").GetComponent<Button>();
	    _gBackToMenu    = GameObject.Find("btnBackToMenu").GetComponent<Button>();
	    _gRegister      = GameObject.Find("btnRegister").GetComponent<Button>();
	    _gLogin         = GameObject.Find("btnBackToLogin").GetComponent<Button>();
	    _gCustomise     = GameObject.Find("btnCharCustomise").GetComponent<Button>();

	    FindMainMenuButtons(GameObject.Find("UI"));

        _gMultiPlayer.onClick.AddListener(MultiplayerButton_OnClick);
        _gBackToMenu.onClick.AddListener(BackToMenu_OnClick);
        _gRegister.onClick.AddListener(Register_OnClick);
        _gLogin.onClick.AddListener(Login_OnClick);
        _gCustomise.onClick.AddListener(CustomiseButton_Click);
	}

    private void FindMainMenuButtons(GameObject parentObject)
    {
        foreach (Transform child in parentObject.transform)
        {
            if (child.gameObject.name == "btnMainMenu") child.GetComponent<Button>().onClick.AddListener(MainMenuButton_OnClick);

            if (child.childCount > 0)
            {
                FindMainMenuButtons(child.gameObject);
            }
        }
    }

    private void MainMenuButton_OnClick()
    {
        GameObject.Find("UI").GetComponent<MenuControl>().ChangeMenu(Menu.MainMenu);
    }

    private void MultiplayerButton_OnClick()
    {
        GameObject.Find("UI").GetComponent<MenuControl>().ChangeMenu(Menu.Multiplayer);
    }

    private void CustomiseButton_Click()
    {
        GameObject.Find("UI").GetComponent<MenuControl>().ChangeMenu(Menu.Customisation);
    }

    private void BackToMenu_OnClick()
    {
        if (Network.isServer)
        {
            MasterServer.UnregisterHost();
        }

        Network.Disconnect();
    }

    private void Register_OnClick()
    {
        GameObject.Find("txtUsername").GetComponentInChildren<Text>().text = String.Empty;
        GameObject.Find("txtPassword").GetComponentInChildren<Text>().text = String.Empty;
        GameObject.Find("lblLoginStatus").GetComponent<Text>().text = String.Empty;
        _menuControl.ChangeMenu(Menu.Registration);
    }

    private void Login_OnClick()
    {
        GameObject.Find("txtUsernameRegister").GetComponentInChildren<Text>().text = String.Empty;
        GameObject.Find("txtPasswordRegister").GetComponentInChildren<Text>().text = String.Empty;
        GameObject.Find("txtEmailRegister").GetComponentInChildren<Text>().text = String.Empty;
        _menuControl.ChangeMenu(Menu.Login);
    }
}
