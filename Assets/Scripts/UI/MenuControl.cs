﻿using System;
using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public enum Menu
{
    MainMenu,
    Multiplayer,
    Options,
    ToGame,
    ToMenu,
    Login,
    Registration,
    Customisation
}

public enum FadeType
{
    FadeOut,
    FadeIn
}

public class MenuControl : MonoBehaviour
{
    public Menu CurMenu;

    private GameObject _gMenuHolder, _gAccountHolder;
    private CanvasGroup _cvsMainMenu, _cvsAccount;
    private Button _btnLogin;
    private List<InputField> inputFields; 

    public bool IsAnimating
    {
        get { return _bAnimating; }
    }

    private float _fAnimateTime = 0.6f;
    private bool _bInGame, _bAnimating;

    public void ChangeMenu(Menu menu)
    {
        StartCoroutine(Animate(menu));
    }

    public void ToggleMenu()
    {
        ChangeMenu(_bInGame ? Menu.ToMenu : Menu.ToGame);
        _bInGame = !_bInGame;
        Debug.Log(String.Format("In Game: {0}", _bInGame));
    }

    private void Start()
    {
        CurMenu         = Menu.Login;

        _gMenuHolder    = GameObject.Find("Main");
        _gAccountHolder = GameObject.Find("Account");

        _cvsMainMenu    = _gMenuHolder.GetComponent<CanvasGroup>();
        _cvsAccount     = _gAccountHolder.GetComponent<CanvasGroup>();

        GameObject.Find("txtPlayerScore").GetComponent<Text>().text     = "Gold: " + PlayerPrefs.GetInt("GEP_PlayerYolk");
        GameObject.Find("txtPlayerRating").GetComponent<Text>().text    = "Rating: " + PlayerPrefs.GetFloat("GEP_PlayerRating");

        StartCoroutine(Fade(FadeType.FadeIn));
    }

    private void FixedUpdate()
    {
        GameObject.Find("txtPlayerScore").GetComponent<Text>().text     = "Gold: " + PlayerPrefs.GetInt("GEP_PlayerYolk");
        GameObject.Find("txtPlayerRating").GetComponent<Text>().text    = "Rating: " + PlayerPrefs.GetFloat("GEP_PlayerRating");
    }

    public IEnumerator Fade(FadeType fadeType)
    {
        var gFadeImage = GameObject.Find("Fader").GetComponent<Image>();
        var cToColor = new Color(0,0,0,0);

        switch (fadeType)
        {
            case FadeType.FadeIn:
                cToColor = new Color(0,0,0,0);
                break;
            case FadeType.FadeOut:
                cToColor = new Color(0,0,0,1);
                break;
        }

        gFadeImage.enabled = true;

        Debug.Log(String.Format("Cur Color: {0},{1},{2},{3}", gFadeImage.color.r, gFadeImage.color.g, gFadeImage.color.b, gFadeImage.color.a));

        var fStartTime = Time.time;
        while (Time.time < fStartTime + 1.5f)
        {
            gFadeImage.color = Color.Lerp(gFadeImage.color, cToColor, (Time.time - fStartTime) / 1.5f);

            yield return null;
        }

        if (fadeType == FadeType.FadeIn) gFadeImage.enabled = false;
    }

    public IEnumerator HandleGameTitle()
    {
        var pnlMainMenu = _gMenuHolder.transform.Find("MainMenu/pnlMenu");
        var pnlTitle    = GameObject.Find("pnlTitle").transform;

        pnlTitle.SetParent(GameObject.Find("UI").transform, true);

        while (_cvsMainMenu.alpha < 1)
        {
            yield return null;
        }

        pnlTitle.SetParent(pnlMainMenu, true);
    }

    private static Vector2 GetMenuVector(Menu menu, RectTransform rRectTransform)
    {
        var vEndVector = Vector2.zero;
        switch (menu)
        {
            case Menu.MainMenu:
                vEndVector = new Vector2(0, 0);
                break;
            case Menu.Login:
                vEndVector = new Vector2(0, 0);
                break;
            case Menu.Multiplayer:
                vEndVector = new Vector2(-1920, 0);
                break;
            case Menu.Registration:
                vEndVector = new Vector2(-1920, 0);
                break;
            case Menu.Customisation:
                vEndVector = new Vector2(1920, 0);
                break;
            case Menu.ToGame:
                vEndVector = new Vector2(rRectTransform.anchoredPosition.x, 1080);
                break;
            case Menu.ToMenu:
                vEndVector = new Vector2(rRectTransform.anchoredPosition.x, 0);
                break;
        }
        return vEndVector;
    }

    public void SetMenu(Menu menu)
    {
        var rRectTransform = GameObject.Find("Menus").GetComponent<RectTransform>();
        rRectTransform.anchoredPosition = GetMenuVector(menu, rRectTransform);
    }

    public void FadeMenu(Menu menu)
    {
        StartCoroutine(Fade(menu));
    }

    private IEnumerator Fade(Menu menu)
    {
        var gFadeFrom = menu == Menu.MainMenu
            ? _cvsAccount
            : _cvsMainMenu;
        var gFadeTo = menu == Menu.MainMenu
            ? _cvsMainMenu
            : _cvsAccount;

        gFadeTo.interactable = true;

        var fStartTime = Time.time;
        while (Time.time < fStartTime + _fAnimateTime)
        {
            gFadeFrom.alpha = Mathf.Lerp(
                1, 0,
                (Time.time - fStartTime) / _fAnimateTime);

            gFadeTo.alpha = Mathf.Lerp(
                0, 1,
                (Time.time - fStartTime) / _fAnimateTime);

            yield return null;
        }

        CurMenu = menu;

        gFadeTo.alpha               = 1;
        gFadeFrom.alpha             = 0;

        gFadeTo.blocksRaycasts      = true;
        gFadeFrom.blocksRaycasts    = false;

        gFadeFrom.interactable      = false;
    }

    private IEnumerator Animate(Menu menu)
    {
        if (_bAnimating) yield return null;

        _bAnimating = true;

        if (menu == Menu.ToMenu)
        {
            GameObject.Find("Menus").GetComponent<CanvasGroup>().interactable = true;
        }
        
        var rRectTransform = GameObject.Find("Menus").GetComponent<RectTransform>();
        var fStartTime = Time.time;

        while (Time.time < fStartTime + _fAnimateTime)
        {
            var vAnchoredPos = rRectTransform.anchoredPosition;

            rRectTransform.anchoredPosition = Vector2.Lerp(
                vAnchoredPos, GetMenuVector(menu, rRectTransform), 
                (Time.time - fStartTime) / _fAnimateTime);

            yield return null;
        }

        rRectTransform.anchoredPosition = GetMenuVector(menu, rRectTransform);
        CurMenu = menu;

        GameObject parentGameObject = null;

        switch (menu)
        {
            case Menu.Login:
                parentGameObject = GameObject.Find("Login");
                break;
            case Menu.Registration:
                parentGameObject = GameObject.Find("Registration");
                break;
        }

        Debug.Log("Refreshing Inputs");
        if (parentGameObject != null)
        {
            gameObject.GetComponent<MenuNavigation>().RefreshInputs(parentGameObject);
        }

        if (menu == Menu.ToGame)
        {
            GameObject.Find("Menus").GetComponent<CanvasGroup>().interactable = false;
        }

        _bAnimating = false;
    }
}
