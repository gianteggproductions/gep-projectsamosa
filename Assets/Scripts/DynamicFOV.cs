﻿using UnityEngine;
using System.Collections;

public class DynamicFOV : MonoBehaviour 
{
	private Camera camera;

	void Start()
	{
		camera = GetComponent<Camera> ();
	}

	void Update ()
	{
	    float fFOV = 16/9f;
        camera.fieldOfView = 50 * fFOV / (camera.pixelWidth / camera.pixelHeight);
	}
}
