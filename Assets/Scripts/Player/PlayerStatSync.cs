﻿using System;
using System.Runtime.InteropServices;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerStatSync : MonoBehaviour
{
    private GameObject _playerOneUi, _playerTwoUi;

	void Start ()
	{
        _playerOneUi = transform.Find("PlayerOne").gameObject;
	    _playerTwoUi = transform.Find("PlayerTwo").gameObject;
	}
	
	void Update ()
	{
	    var statsPlayerOne = GameObject.Find("Player(Clone)");
	    if (statsPlayerOne == null) return;
	    float playerOneHP;
	    try
	    {
	        playerOneHP = statsPlayerOne.GetComponent<PlayerStats>().fHealth;
	    }
	    catch (NullReferenceException)
	    {
	        return;
	    }

	    _playerOneUi.transform.Find("Health").GetComponent<Slider>().value = playerOneHP;

	    var statsPlayerTwo = GameObject.Find("OtherPlayer(Clone)");
	    if (statsPlayerTwo == null) return;
        float playerTwoHP;
        try
        {
            playerTwoHP = statsPlayerOne.GetComponent<PlayerStats>().fHealth;
        }
        catch (NullReferenceException)
        {
            return;
        }

	    _playerTwoUi.transform.Find("Health").GetComponent<Slider>().value = playerTwoHP;
	}
}
