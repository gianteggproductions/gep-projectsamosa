﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerInput : MonoBehaviour
{
    public bool bPlayerInputAllowed = true;

    private Text _txtPlayerSpell;
    private GameObject _gPlayer;
	private NetworkView _netView;

    private void Start()
    {
        Debug.Log("Input Start");
        _txtPlayerSpell = transform.Find("cvsPlayerSpell/txtPlayerSpell").GetComponent<Text>();
        _gPlayer = gameObject;

		_netView = GetComponent<NetworkView> ();
    }

    private void Update ()
    {
        if (!_netView.isMine) return;

        if (Input.GetKeyUp(KeyCode.Escape))
        {
            var menuControl = GameObject.Find("UI").GetComponent<MenuControl>();

            if (menuControl.IsAnimating) return;

            Debug.Log("Escape Captured");
            menuControl.ToggleMenu();
            return;
        }
        
        if (Input.GetKeyUp(KeyCode.Space))
        {
            _gPlayer.GetComponent<PlayerAbilities>().Jump();
            return;
        }

        if (!bPlayerInputAllowed) return;
        if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)) return;

        foreach (char c in Input.inputString)
        {
            _txtPlayerSpell.text = _txtPlayerSpell.text.Trim();

	        if (c == "\b"[0] && _txtPlayerSpell.text.Length > 0)
	        {
	            _txtPlayerSpell.text = _txtPlayerSpell.text.Substring(0, _txtPlayerSpell.text.Length - 1);
	        }
            else if ((c == "\n"[0] || c == "\r"[0]) && !String.IsNullOrEmpty(_txtPlayerSpell.text))
            {
                Debug.Log("Player Entered Spell: " + _txtPlayerSpell.text);
                _gPlayer.GetComponent<PlayerAbilities>().HandleSpell(_txtPlayerSpell.text);
                _txtPlayerSpell.text = String.Empty;
            }
            else if (c == "\t"[0])
            {
                Debug.Log("Handled Space");
                return;
            }
            else
            {
                Debug.Log("Player Entered " + c);
                var newText = _txtPlayerSpell.text.Trim();
                if (String.IsNullOrEmpty(newText))
                {
                    _txtPlayerSpell.text += Char.ToUpper(c);
                }
                else
                {
                    _txtPlayerSpell.text += c;
                }
            }

            var gPlayerObject = Network.isClient ? GameObject.Find("OtherPlayer(Clone)") : GameObject.Find("Player(Clone)");
            var playerSerialisation = gPlayerObject.GetComponent<PlayerSerialization>();
            var sPlayerSpell = gPlayerObject.transform.Find("cvsPlayerSpell/txtPlayerSpell").GetComponent<Text>().text;

            if (Network.isClient)
            {
                playerSerialisation.SetTextP1(sPlayerSpell);
            }
            else
            {
                playerSerialisation.SetTextP2(sPlayerSpell);
            }
	    }
	}
}
