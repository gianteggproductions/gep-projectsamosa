﻿using UnityEngine;
using System.Collections;

public class PlayerPhysics : MonoBehaviour {

    public bool bIsPlayerGrounded
    {
        get
        {
            RaycastHit hit;
            if (!Physics.Raycast(transform.position, Vector3.down, out hit, .5f)) return false;

            return hit.collider.gameObject.name != "Player";
        }
    }
}
