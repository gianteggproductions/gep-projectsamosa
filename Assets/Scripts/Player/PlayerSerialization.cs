﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerSerialization : MonoBehaviour {

    private float _fSyncTime, _fSyncDelay, _fLastSync;
    private Vector3 _vSyncStart, _vSyncEnd;

    public void SetTextP1(String text)
    {
        networkView.RPC("OnRecieveTextUpdateP1", RPCMode.Others, text);
    }

    public void SetTextP2(String text)
    {
        networkView.RPC("OnRecieveTextUpdateP2", RPCMode.Others, text);
    }

    [RPC]
    private void OnRecieveTextUpdateP1(String text)
    {
        GameObject.Find("OtherPlayer(Clone)").transform.Find("cvsPlayerSpell/txtPlayerSpell").GetComponent<Text>().text = text;
    }

    [RPC]
    private void OnRecieveTextUpdateP2(String text)
    {
        GameObject.Find("Player(Clone)").transform.Find("cvsPlayerSpell/txtPlayerSpell").GetComponent<Text>().text = text;
    }

    private void OnSerializeNetworkView(BitStream stream, NetworkMessageInfo info)
    {
        Debug.Log("Serializing...");
        var syncPosition = Vector3.zero;
        if (stream.isWriting)
        {
            syncPosition = rigidbody.position;
            stream.Serialize(ref syncPosition);
        }
        else
        {
            stream.Serialize(ref syncPosition);

            _fSyncTime = 0f;
            _fSyncDelay = Time.time - _fLastSync;
            _fLastSync = Time.time;

            _vSyncStart = rigidbody.position;
            _vSyncEnd = syncPosition;
        }
    }

    private void Update()
    {
        if (networkView.isMine) return;

        _fSyncTime += Time.deltaTime;
        rigidbody.position = Vector3.Lerp(_vSyncStart, _vSyncEnd, _fSyncTime / _fSyncDelay);
    }
}
