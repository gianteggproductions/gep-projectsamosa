﻿using System;
using UnityEngine;
using System.Collections;

public class PlayerAbilities : MonoBehaviour
{

    private float _fJumpSpeed = 5000f;
    private const int _iMaxJumps = 2;
    private int _iCurJumps;
    private bool _bIsJumping;
    private Transform _tSpellPos;
    private GameObject _gFireballPrefab, _gDeflectPrefab;
	private Rigidbody _rigidbody;

    private PlayerPhysics _playerPhysics;

    private void Start()
    {
        _tSpellPos = transform.FindChild("SpellPos");
        _playerPhysics = gameObject.GetComponent<PlayerPhysics>();

        _gFireballPrefab = Resources.Load<GameObject>("Prefabs/Spells/Fireball");
        _gDeflectPrefab = Resources.Load<GameObject>("Prefabs/Spells/Deflect");

		_rigidbody = GetComponent<Rigidbody> ();
    }

    private void Update()
    {
        HandleJump();
        HandleKeyCombos();
    }

    private void HandleJump()
    {
        if (!_bIsJumping || !_playerPhysics.bIsPlayerGrounded) return;

        _iCurJumps = 0;
        _bIsJumping = false;
        _fJumpSpeed = 5000f;

        Debug.Log("Hit Floor");
    }

    private void HandleKeyCombos()
    {
        if ((Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)) && Input.GetKeyDown(KeyCode.D))
        {
            HandleSpell("Deflect");
        }
    }

    public void Jump( )
    {
        if (_iCurJumps >= _iMaxJumps) return;

        if (_iCurJumps == 1) _fJumpSpeed = 1500f;

        _rigidbody.velocity = Vector3.zero;
        _rigidbody.AddForce(Vector3.up * _fJumpSpeed);
        Debug.Log("Jumped");

        _iCurJumps++;

        StartCoroutine(WaitForJump());
    }

    private IEnumerator WaitForJump()
    {
        while (_rigidbody.position.y < 0)
        {
            yield return null;
        }

        _bIsJumping = true;
    }

    private void Fireball()
    {
        Debug.Log("Player cast Fireball");

        Quaternion _qFaceDirection;

        if (Network.isClient)
        {
            _qFaceDirection = Quaternion.Euler(0, 270, 0);
        }
        else
        {
            _qFaceDirection = Quaternion.Euler(0, 90, 0);
        }

        Network.Instantiate(_gFireballPrefab, _tSpellPos.position, _qFaceDirection, 2);
    }

    private void Deflect()
    {
        Debug.Log("Player cast Deflect");
        var vOffset = new Vector3(gameObject.name == "Player(Clone)" ? 0.5f : -0.5f, 0.25f, 0f);
        Network.Instantiate(_gDeflectPrefab, _tSpellPos.position + vOffset, Quaternion.identity, 2);
    }

    public void HandleSpell(String spellName)
    {
        var spell = spellName.ToLower().Trim();
        switch (spell)
        {
            case "fireball":
                Fireball();
                break;
            case "deflect":
                Deflect();
                break;
        }

        Debug.Log(String.Format("Handled Spell: {0}", spellName));
    }
}
