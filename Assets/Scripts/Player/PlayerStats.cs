﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerStats : MonoBehaviour
{
    public float fHealth = 100f, fMana = 100f;
    private float _fMaxHealth = 100f, _fMaxMana = 100f;
    private MenuControl _pMenuControl;
    private Text _tMatchStatus, _tMatchRewards;

    private void Start()
    {
        _pMenuControl = GameObject.Find("UI").GetComponent<MenuControl>();
        _tMatchStatus = GameObject.Find("txtMatchStatus").GetComponent<Text>();
        _tMatchRewards = GameObject.Find("txtRewards").GetComponent<Text>();
    }

    public void DamagePlayer(float fDamage)
    {
        // Clamp the damage to the current health so we don't need to deal with negative numbers
        fHealth -= Mathf.Clamp(fDamage, 0, fHealth);
        Debug.Log("Health: " + fHealth);
        networkView.RPC("SyncHealth", RPCMode.All, fHealth);

        Debug.Log("Calling Flash Damage");
        networkView.RPC("RPCFlashDamage", RPCMode.All, 0.1f);

        if (fHealth <= 0)
        {
            networkView.RPC("Die", RPCMode.All);
        }
    }

    public void Win()
    {
        _tMatchStatus.text = "You Win";
        _tMatchRewards.color = Color.green;
        _tMatchRewards.text = "+25 Yolk\r\nSkill Rating Increased";

        PlayerPrefs.SetInt("GEP_PlayerYolk", PlayerPrefs.GetInt("GEP_PlayerYolk") + 25);
        PlayerPrefs.SetFloat("GEP_PlayerRating", PlayerPrefs.GetFloat("GEP_PlayerRating") + 0.1f);

        StartCoroutine(GameOver());
    }

    public void Lose()
    {
        _tMatchStatus.text = "You Lost";
        _tMatchRewards.color = Color.red;
        _tMatchRewards.text = "-10 Yolk\r\nP.S. ur probably shit";

        PlayerPrefs.SetFloat("GEP_PlayerRating", PlayerPrefs.GetFloat("GEP_PlayerRating") - 0.1f);

        StartCoroutine(GameOver());
    }

    private IEnumerator GameOver()
    {
        StartCoroutine(_pMenuControl.Fade(FadeType.FadeOut));

        yield return new WaitForSeconds(.5f);

        var gGameOverUI = GameObject.Find("GameOver").GetComponent<CanvasGroup>();
        gGameOverUI.interactable = true;

        var fStartTime = Time.time;
        while (Time.time < fStartTime + 1f)
        {
            gGameOverUI.alpha = Mathf.Lerp(0, 1, (Time.time - fStartTime) / 1f);

            yield return null;
        }
    }

    private IEnumerator FlashDamage(float fTime)
    {
        var gChildObjects = gameObject.GetComponentsInChildren<MeshRenderer>();

        Color cRendererColor;
        var fStartTime = Time.time;
        while (Time.time < fStartTime + fTime)
        {
            cRendererColor = Color.Lerp(renderer.material.GetColor("_Color"), Color.red, (Time.time - fStartTime) / fTime);

            foreach (var childRenderer in gChildObjects)
            {
                childRenderer.material.SetColor("_Color", cRendererColor);
            }

            yield return null;
        }

        fStartTime = Time.time;
        while (Time.time < fStartTime + fTime)
        {
            cRendererColor = Color.Lerp(renderer.material.GetColor("_Color"), Color.white, (Time.time - fStartTime) / fTime);

            foreach (var childRenderer in gChildObjects)
            {
                childRenderer.material.SetColor("_Color", cRendererColor);
            }

            yield return null;
        }
    }

    [RPC]
    private void SyncHealth(float newHealth)
    {
        fHealth = newHealth;
    }

    [RPC]
    private void Die()
    {
        Debug.Log("Game Ended");

        if (networkView.isMine)
        {
            gameObject.GetComponent<PlayerInput>().bPlayerInputAllowed = false;
            Lose();
        }
        else
        {
            Win();
            var sPlayer = gameObject.name;

            if (sPlayer == "Player(Clone)")
            {
                GameObject.Find("OtherPlayer(Clone)").GetComponent<PlayerInput>().bPlayerInputAllowed = false;
            }
            else
            {
                GameObject.Find("Player(Clone)").GetComponent<PlayerInput>().bPlayerInputAllowed = false;
            }
        }
    }

    [RPC]
    private void RPCFlashDamage(float fTime)
    {
        StartCoroutine(FlashDamage(fTime));
    }
}
