﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerNetworkManager : MonoBehaviour
{
    public bool bIsConnectedToServer = false;

    private const string _sGameName = "GEP_ProjectSamosa";
    private const float _fRefreshInterval = 3f;
    private bool _bIsRefreshing;

    private GameObject _gPlayer, _gOtherPlayer, _gMainCamera, _gEventSystem;
    private HostData[] _hostData;
    private ServerPanelHandler _serverPanel;
    private PlayerInput _playerInput;
    private Notification _notification;
    private PlayerUIControl _playerUiControl;
    private MenuControl _pMenuControl;

    #region Unity Methods

    private void Start()
    {
        _serverPanel  = GameObject.Find("pnlServerList").GetComponent<ServerPanelHandler>();
        _notification = GameObject.Find("NotificationHandler").GetComponent<Notification>();
        _playerUiControl = GameObject.Find("UI").GetComponent<PlayerUIControl>();
        _pMenuControl = GameObject.Find("UI").GetComponent<MenuControl>();

        // Load resources privately so we expose less scripts publicly
        _gPlayer = Resources.Load<GameObject>("Prefabs/Player");
        _gOtherPlayer = Resources.Load<GameObject>("Prefabs/OtherPlayer");

        _gMainCamera = GameObject.FindGameObjectWithTag("MainCamera");
        _gEventSystem = GameObject.Find("EventSystem");

        Debug.Log(String.Format("Screen Size: {0}x{1}", Screen.width, Screen.height));
    }

    #endregion

    #region Custom Methods

    public void StartServer(string hostName)
    {
        if (String.IsNullOrEmpty(hostName))
        {
            _notification.DisplayNotification("Empty Host Name Given!");
            return;
        }

        Action startServerAction =
            delegate
            {
                DontDestroyOnLoad(_gMainCamera);
                DontDestroyOnLoad(_gEventSystem);

                Application.LoadLevel("Castle");

                foreach (var gObject in GameObject.FindGameObjectsWithTag("_ScriptDestroy"))
                {
                    Destroy(gObject);
                }

                Debug.Log("Starting Up Server...");

                // Default port of 25002, however hardly anyone has these ports forwarded
                // so we'll need to find a better way to do peer-to-peer hosting
                Network.InitializeServer(2, 25002, true);

                MasterServer.RegisterHost(_sGameName, hostName);
                _serverPanel.SetServerToggleText("Stop Server");

                StartCoroutine(WaitForLoad(Player.One));

                GameObject.Find("UI").GetComponent<MenuControl>().ChangeMenu(Menu.ToGame);
            };

        StartCoroutine(FadeOut(startServerAction));
    }

    public void StopServer()
    {
        Action stopServerAction = 
            delegate 
            {
                Debug.Log("Shutting Down Server...");

                Destroy(GameObject.Find("Main Camera"));
                Destroy(GameObject.Find("EventSystem"));

                // If we don't Network.Destroy the player objects they stay in-game
                // meaning we'd have new player objects spawning inside of the old objects
                var gPlayers = GameObject.FindGameObjectsWithTag("Player");
                foreach (var gPlayerObject in gPlayers)
                {
                    Network.Destroy(gPlayerObject);
                }

                Network.Disconnect();
                MasterServer.UnregisterHost();

                _serverPanel.SetServerToggleText("Start Server");

                Application.LoadLevel("Main");

                _pMenuControl.ChangeMenu(Menu.ToMenu);
            };

        StartCoroutine(FadeOut(stopServerAction));
    }

    public void ConnectToServer(HostData hostData)
    {
        Action connectToServerAction =
            delegate
            {
                if (Network.isServer)
                {
                    _notification.DisplayNotification("You're already the host of another server");
                    return;
                }

                DontDestroyOnLoad(_gMainCamera);
                DontDestroyOnLoad(_gEventSystem);

                Application.LoadLevel("Castle");

                Network.Connect(hostData);
                Debug.Log(String.Format("Connecting to server {0} with {1} {2}", hostData.gameName, hostData.connectedPlayers, hostData.connectedPlayers == 1 ? "player" : "players"));
            };

        StartCoroutine(FadeOut(connectToServerAction));
    }

    public void RefreshServerList()
    {
        StartCoroutine(RefreshHosts());
    }

    #endregion

    #region Enumerators

    private IEnumerator FadeOut(Action dFuncToRun)
    {
        StartCoroutine(_pMenuControl.Fade(FadeType.FadeOut));

        yield return new WaitForSeconds(.5f);

        dFuncToRun();
    }

    private IEnumerator WaitForLoad(Player player)
    {
        while (Application.isLoadingLevel)
        {
            yield return null;
        }

        switch (player)
        {
            case Player.One:
                Debug.Log("Spawning Player One");
                Network.Instantiate(_gPlayer, GameObject.Find("SpawnPointOne").transform.position, Quaternion.Euler(0, 270, 0), 0);
                break;
            case Player.Two:
                Debug.Log("Spawning Player Two");
                Network.Instantiate(_gOtherPlayer, GameObject.Find("SpawnPointTwo").transform.position, Quaternion.Euler(0, 90, 0), 1);
                _gOtherPlayer.transform.Find("cvsPlayerSpell").GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 270, 0);
                break;
        }

        yield return new WaitForSeconds(1);

        StartCoroutine(_pMenuControl.Fade(FadeType.FadeIn));
    }

    private IEnumerator RefreshHosts()
    {
        if (_bIsRefreshing) yield break;

        _bIsRefreshing = true;
        var serverPanel = GameObject.Find("pnlServerList").GetComponent<ServerPanelHandler>();
        serverPanel.SetBrowserStatusText("Refreshing Server List...");
        serverPanel.ClearServerList();

        Debug.Log("Refreshing Server List...");

        MasterServer.RequestHostList(_sGameName);

        float fTimeStarted = Time.time, fTimeEnd = fTimeStarted + _fRefreshInterval;

        // By default, we give the master server 3 seconds to respond with the host list
        // This should be enough time for every single server to stream info to the client
        while (Time.time < fTimeEnd)
        {
            _hostData = MasterServer.PollHostList();
            yield return new WaitForEndOfFrame();
        }

        if (_hostData == null || _hostData.Length == 0)
        {
            serverPanel.SetBrowserStatusText("No Servers Found :(");
        }
        else
        {
            serverPanel.SetBrowserStatusText("");

            foreach (var hostData in _hostData)
            {
                serverPanel.AddNewServer(hostData);
            }
        }

        _bIsRefreshing = false;
    }

    #endregion

    #region Server Events

    private void OnServerInitialized()
    {
        // Basic server init code, we'd need to setup proper wait times for another player to join
        Debug.Log("Server Initialised.");

        if (_gPlayer == null) Debug.Log("Player Is Null");

        _playerUiControl.ActivatePlayer(Player.One);
    }

    private void OnDisconnectedFromServer(NetworkDisconnection info)
    {
        Action onDisconnectAction =
            delegate
            {
                Destroy(_gMainCamera);
                Destroy(_gEventSystem);

                Application.LoadLevel("Main");

                if (Network.isServer)
                {
                    _notification.DisplayNotification("Server has shut down");
                    Debug.Log("Server Shut Down");
                }
                else
                {
                    if (info == NetworkDisconnection.Disconnected)
                    {
                        _notification.DisplayNotification("Disconnected from server");
                    }
                    else
                    {
                        _notification.DisplayNotification("Lost connection to server");
                    }
                    Debug.Log(info == NetworkDisconnection.LostConnection ? "Lost Connection" : "Disconnected From Server");
                }

                _playerUiControl.DeactivatePlayer(Player.One);
                _playerUiControl.DeactivatePlayer(Player.Two);
                _pMenuControl.ChangeMenu(Menu.ToMenu);

                // If the server crashes or the host alt+f4s the server shutdown never gets fired,
                // instead it just closes all networks and we need to destroy clientside objects manually
                var gPlayers = GameObject.FindGameObjectsWithTag("Player");
                foreach (var player in gPlayers)
                {
                    Destroy(player);
                }
            };

        StartCoroutine(FadeOut(onDisconnectAction));
    }

    private void OnMasterServerEvent(MasterServerEvent msEvent)
    {
        // This event fires randomly every 20 or so seconds, gotta figure out why
        // (Probably when connected players are registered on the master server)
        if (msEvent == MasterServerEvent.RegistrationSucceeded)
        {
            Debug.Log("Server Registered With Master Server");
        }
    }

    private void OnConnectedToServer()
    {
        _notification.DisplayNotification("You are now connected");
        Debug.Log("Connected");
        bIsConnectedToServer = true;

        GameObject.Find("UI").GetComponent<MenuControl>().ChangeMenu(Menu.ToGame);

        StartCoroutine(WaitForLoad(Player.Two));

        // If someone is connecting to a server, that means one player is already on the server
        // so activate both sets of UI which denote both player's stats
        _playerUiControl.ActivatePlayer(Player.One);
        _playerUiControl.ActivatePlayer(Player.Two);
    }

    private void OnPlayerConnected(NetworkPlayer netPlayer)
    {
        Debug.Log(String.Format("Player connected with IP: {0} ({1} {2})", netPlayer.ipAddress, Network.connections.Count(), Network.connections.Count() == 1 ? "player" : "players"));

        _playerUiControl.ActivatePlayer(Player.Two);
    }

    private void OnPlayerDisconnected(NetworkPlayer netPlayer)
    {
        // Here we remove all network calls and objects so that everything the player
        // has called/instantiated is cleaned up for the next player to join into a fresh server
        Network.RemoveRPCs(netPlayer);
        Network.DestroyPlayerObjects(netPlayer);

        _playerUiControl.DeactivatePlayer(Player.Two);
    }

    private void OnFailedToConnect(NetworkConnectionError netError)
    {
        var sError = "";

        switch (netError)
        {
            case NetworkConnectionError.InternalDirectConnectFailed:
                sError = "Server is no longer available";
                break;
            case NetworkConnectionError.ConnectionBanned:
                sError = "You're banned from this server";
                break;
            case NetworkConnectionError.AlreadyConnectedToAnotherServer:
                sError = "You're connected to another server";
                break;
            case NetworkConnectionError.AlreadyConnectedToServer:
                sError = "You're already connected to the server";
                break;
            case NetworkConnectionError.ConnectionFailed:
                sError = "Connect Failed";
                break;
            case NetworkConnectionError.TooManyConnectedPlayers:
                sError = "Server is full";
                break;
            case NetworkConnectionError.NATPunchthroughFailed:
                sError = "Failed to punch-through your NAT Settings\r\n" +
                         "The host most likely hasn't forwarded their ports";
                break;
        }

        _notification.DisplayNotification(String.Format("Failed To Connect \r\n\r\n{0}", String.IsNullOrEmpty(sError) ? netError.ToString() : sError));
    }

    #endregion
}
