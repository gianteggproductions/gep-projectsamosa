﻿using System;
using System.Globalization;
using UnityEngine;
using System.Collections;

public class Fireball : MonoBehaviour
{
    private float _fMovementSpeed = 10f, _fSyncTime, 
                  _fSyncDelay, _fLastSync;
    private Vector3 _vSyncStart, _vSyncEnd;

	private void Start () 
    {
        Debug.Log(gameObject.networkView.viewID);

	    var particleSysPos = transform.Find("Particle System").localPosition;
        transform.Find("Particle System").localPosition = new Vector3(particleSysPos.x, particleSysPos.y + 0.4f, particleSysPos.z);
    }
	
	private void Update ()
	{
	    if (networkView.isMine)
	    {
	        transform.position += transform.forward * Time.deltaTime * _fMovementSpeed;
	    }
	    else
	    {
	        _fSyncTime += Time.deltaTime;
            rigidbody.position = Vector3.Lerp(_vSyncStart, _vSyncEnd, _fSyncTime / _fSyncDelay);
	    }
	}

    private void OnSerializeNetworkView(BitStream stream, NetworkMessageInfo info)
    {
        Debug.Log("Serializing Fireball...");
        var syncPosition = Vector3.zero;
        if (stream.isWriting)
        {
            syncPosition = rigidbody.position;
            stream.Serialize(ref syncPosition);
        }
        else
        {
            stream.Serialize(ref syncPosition);

            _fSyncTime = 0f;
            _fSyncDelay = Time.time - _fLastSync;
            _fLastSync = Time.time;

            _vSyncStart = rigidbody.position;
            _vSyncEnd = syncPosition;
        }
    }

    private void OnTriggerEnter(Collider collider)
    {
        if (collider.tag == "Player")
        {
            collider.GetComponent<PlayerStats>().DamagePlayer(25f);
        }

        if (collider.tag != "Player" && collider.tag != "Bounds") return;

        //networkView.RPC("Destroy", RPCMode.All);
        if (networkView.isMine)
        {
            Network.Destroy(gameObject.networkView.viewID);
        }
    }

    [RPC]
    private void Destroy()
    {
        Destroy(gameObject);
    }
}
