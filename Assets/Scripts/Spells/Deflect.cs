﻿using UnityEngine;
using System.Collections;

public class Deflect : MonoBehaviour
{

    private readonly float _fStartTime = Time.time;
    private const float _fDuration = 7f;

    private void Update () {
	    if (Time.time > _fStartTime + _fDuration)
	    {
            Destroy(gameObject);
	    }
	}

    private void OnTriggerEnter(Collider collisionObject)
    {
        if (collisionObject.tag != "Spell") return;
        if (collisionObject.networkView.isMine) return;

        Debug.Log("Deflecting Spell");
        collisionObject.transform.Rotate(Vector3.up, 180, Space.World);
    }
}
